<?php

namespace kusior\TerytBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ZaladujGminyCommand extends ContainerAwareCommand
{	/**
     * Initialize whatever variables you may need to store beforehand, also load
         * Doctrine from the Container
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output); //initialize parent class method

        $this->em = $this->getContainer()->get('doctrine')->getManager(); // This loads Doctrine, you can load your own services as well
    }
     /**
    * Configure the task with options and arguments
    */
    protected function configure()
    {
        parent::configure();

        $this

        ->setName('teryt:zaladuj-gminy') // this is the command you would pass to console to run the command.
        ->setDescription('Laduje gminy, powiaty oraz wojewodztwa do bazy danych')
        ->setDefinition(array(
                new InputArgument('sciezka', InputArgument::REQUIRED, 'Sciezka do pliku TERC.xml')
            )
        );    
    }
     /**
    * Our console/task logic
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sciezka   = $input->getArgument('sciezka');

        $XMLReader = new \XMLReader();
        $XMLReader->open($sciezka);

        while ($XMLReader->read()) {
            //otwarcie cola
            if ($XMLReader->name == 'col' && $XMLReader->nodeType == \XMLReader::ELEMENT) {
                if (isset($key)) {
                   $obj[$key]='';
                }
                $key = $XMLReader->getAttribute('name');
            }
            if ($XMLReader->nodeType == \XMLReader::TEXT) {
                if (isset($key)) {
                    $obj[$key] = $XMLReader->value;
                }
            }
            if ($XMLReader->name == 'col' && $XMLReader->nodeType == \XMLReader::END_ELEMENT) {
                if (isset($key)) {
                    unset($key);
                }
            } elseif ($XMLReader->name == 'row' && $XMLReader->nodeType == \XMLReader::END_ELEMENT) {
                if (isset($obj)) {
                    if ($obj['POW']=='') {
                        $class = new \kusior\TerytBundle\Entity\Wojewodztwo();
                    } elseif ($obj['GMI']=='') {
                        $class = new \kusior\TerytBundle\Entity\Powiat();
                    } else {
                        $class = new \kusior\TerytBundle\Entity\Gmina();
                    }
                    $class->loadDataFromArray($obj);
                    unset($obj);
                    $this->em->persist($class);
                }
            }
        }
        $XMLReader->close();
        $this->em->flush();
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('sciezka')) {
            $sciezka = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Podaj ścieżkę do pliku XML:',
                function($sciezka) {
                    if (empty($sciezka)) {
                        throw new \Exception('Nie podano ścieżki');
                    }

                    return $sciezka;
                }
            );
            $input->setArgument('sciezka', $sciezka);
        }
    }
}
