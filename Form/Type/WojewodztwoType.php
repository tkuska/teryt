<?php

namespace kusior\TerytBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class WojewodztwoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'wojewodztwo';
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'powiat',
            'gmina',
            'miejscowosc'
        ));
    }
    
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options) {
        
        parent::buildView($view, $form, $options);
        
        $view->vars = array_replace($view->vars, array(
            'powiatid'  => $view->parent->vars['id']."_".$options['powiat'],
            'gminaid'     => $view->parent->vars['id']."_".$options['gmina'],
            'miejscowoscid'      => $view->parent->vars['id']."_".$options['miejscowosc'],            
        ));
    }
}
