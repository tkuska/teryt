<?php

namespace kusior\TerytBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class UlicaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ulica';
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'miejscowosc',
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options) {
        
        parent::buildView($view, $form, $options);
        
        $view->vars = array_replace($view->vars, array(
            'miejscowoscid'  => $view->parent->vars['id']."_".$options['miejscowosc'],         
        ));
    }
}
