<?php

namespace kusior\TerytBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class PowiatType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'powiat';
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'gmina',
            'wojewodztwo',
            'miejscowosc'
        ));
    }
    
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options) {
        
        parent::buildView($view, $form, $options);
        
        $view->vars = array_replace($view->vars, array(
            'gminaid'  => $view->parent->vars['id']."_".$options['gmina'],
            'wojewodztwoid'     => $view->parent->vars['id']."_".$options['wojewodztwo'],
            'miejscowoscid'      => $view->parent->vars['id']."_".$options['miejscowosc'],            
        ));
    }
}
