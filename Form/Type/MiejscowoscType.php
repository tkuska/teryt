<?php

namespace kusior\TerytBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class MiejscowoscType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'miejscowosc';
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'powiat',
            'wojewodztwo',
            'gmina'
        ));
    }
    
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options) {
        
        parent::buildView($view, $form, $options);
        
        $view->vars = array_replace($view->vars, array(
            'powiatid'  => $view->parent->vars['id']."_".$options['powiat'],
            'wojewodztwoid'     => $view->parent->vars['id']."_".$options['wojewodztwo'],
            'gminaid'      => $view->parent->vars['id']."_".$options['gmina'],            
        ));
    }
}
