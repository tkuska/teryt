<?php

namespace kusior\TerytBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Akcja controller.
 *
 * @Route("/teryt")
 */
class TerytController extends Controller
{
    /**
     * Lists all Wojewodztwa entities.
     *
     * @Route("/wojewodztwa/{term}", name="teryt_wojewodztwa", defaults={"term"=""})
     * @Template()
     */
    public function wojewodztwaAction($term = "")
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("kusiorTerytBundle:Wojewodztwo")
                ->getWojewodztwoByName($term);

        return new Response(json_encode($entities));
    }

    /**
     * Lists all Powiaty entities.
     *
     * @Route("/powiaty/{term}/{wojewodztwo}", name="teryt_powiaty", defaults={"term"="", "wojewodztwo"="00"})
     * @Template()
     */
    public function powiatyAction($term = "", $wojewodztwo = "00")
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("kusiorTerytBundle:Powiat")
                ->getPowiatByName($term, $wojewodztwo);

        return new Response(json_encode($entities));
    }

    /**
     * Lists all Gminy entities.
     *
     * @Route("/gminy/{term}/{wojewodztwo}/{powiat}", name="teryt_gminy", defaults={"term"="", "wojewodztwo"="00", "powiat"="00"})
     * @Template()
     */
    public function gminyAction($term = "", $wojewodztwo = "00", $powiat="00")
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("kusiorTerytBundle:Gmina")
                ->getGminaByName($term, $wojewodztwo, $powiat);

        return new Response(json_encode($entities));
    }

    /**
     * Lists all Gminy entities.
     *
     * @Route("/miasta/{term}/{wojewodztwo}/{powiat}/{gmina}", name="teryt_miasta", defaults={"term"="", "wojewodztwo"="00", "powiat"="00", "gmina"="00"})
     * @Template()
     */
    public function miastaAction($term = "", $wojewodztwo = "00", $powiat="00", $gmina="00")
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("kusiorTerytBundle:Miejscowosc")
                ->getMiejscowoscByName($term, $wojewodztwo, $powiat, $gmina);

        return new Response(json_encode($entities));
    }

    /**
     * Lists all Ulice entities.
     *
     * @Route("/ulice/{term}/{wojewodztwo}/{powiat}/{gmina}/{rodzaj_gminy}", name="teryt_ulice", defaults={"term"="", "wojewodztwo"="00", "powiat"="00", "gmina"="00", "rodzaj_gminy"="0"})
     * @Template()
     */
    public function uliceAction($term = "", $wojewodztwo = "00", $powiat="00", $gmina="00", $rodzaj_gminy="0")
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("kusiorTerytBundle:Ulica")
                ->getUlicaByName($term, $wojewodztwo, $powiat, $gmina, $rodzaj_gminy);

        return new Response(json_encode($entities));
    }
}
